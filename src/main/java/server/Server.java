package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Natali on 20/03/14.
 */
public class Server implements Runnable {

    final static int LIMITOFCONNECTIONS = 1000;
    private boolean running;
    ServerSocket serverSocket; //= null;

    private int port;

    public Server(int port) {
        this.port = port;
    }

    @Override
    public void run() {

        System.out.println("Starting the socket server at port:" + port);

        if (!running) {

            ExecutorService executorService = Executors.newFixedThreadPool(LIMITOFCONNECTIONS);
            ServerSocket serverSocket = null;

            try {
                serverSocket = new ServerSocket(port);

                running = true;
                System.out.println("Waiting for clients...");
                while (true) {
                    Socket client = serverSocket.accept();
                    executorService.execute(new ConnectionHandler(client));
                }

            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Problems with server - be patient.");
            }
        } else System.out.println("Server already running.");
    }

    public void stopServer() {
        try {
            if (serverSocket != null) serverSocket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Server was stopped.");
        running = false;
    }

    public boolean isRunning() {
        return running;
    }

    public static void main(String[] args) {

        Server socketServer = new Server(9900);
        socketServer.run();
        socketServer.stopServer();

    }


}

