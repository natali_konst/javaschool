package server;

import common.Command;
import common.dto.ErrorDto;
import server.services.CommandProcessor;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Created by Natali on 20/03/14.
 */

public class ConnectionHandler implements Runnable {

    private Socket socket;

    public ConnectionHandler(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {

        ObjectInputStream ois = null;
        ObjectOutputStream oos = null;

        try {
            ois = new ObjectInputStream(socket.getInputStream());
            oos = new ObjectOutputStream(socket.getOutputStream());

            while (true) {
                Object received = null;
                try {

                    received = ois.readObject();

                } catch (ClassNotFoundException e) {
                    System.out.println("broke");
                    e.printStackTrace();
                }

                Object output;

                if ((received != null) && (received instanceof Command)) {
                    CommandProcessor commandProcessor = new CommandProcessor((Command) received);
                    output = commandProcessor.process();
                } else {
                    output = new ErrorDto("Incorrect command");
                }

                System.out.println(output.toString());

                //TestObject to = new TestObject(1, "object from server");
                oos.writeObject(output);
                oos.flush();
            }

        } catch (Exception e)

        {
            throw new RuntimeException(e);
        } finally {
            try {
                if (ois != null) ois.close();
                if (oos != null) oos.close();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

//
//    private void sendMessage(Socket client, String message) throws IOException {
//        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
//        writer.write(message);
//        writer.flush();
//        writer.close();
//    }
//
//    public String getMessage(Socket client) throws IOException {
//        BufferedReader stdIn = new BufferedReader(new InputStreamReader(client.getInputStream()));
//        System.out.println("Response from client:");
//        return stdIn.readLine();
//    }
}
