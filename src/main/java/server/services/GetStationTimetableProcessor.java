package server.services;

import common.command.GetStationTimetable;
import common.dto.ScheduleDto;
import common.dto.StationDto;
import common.dto.TrainDto;
import server.dao.daoImpl.ScheduleDaoImpl;
import server.models.Schedule;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Natali on 28/03/14.
 */
public class GetStationTimetableProcessor {


    private GetStationTimetable inputCommand;

    public GetStationTimetableProcessor(GetStationTimetable inputCommand) {
        this.inputCommand = inputCommand;
    }

    public Object process() {

        ScheduleDaoImpl scheduleDao = new ScheduleDaoImpl();

        List<ScheduleDto> scheduleDtos = new ArrayList<ScheduleDto>();

        List<Schedule> schedules = scheduleDao.findSchedulePerStation(inputCommand.getStationId());

        for (Schedule schedule : schedules) {
            TrainDto trainDto = new TrainDto(schedule.getIdTrain().getId(), schedule.getIdTrain().getTrainNumber(), schedule.getIdTrain().getNumPlaces());
            StationDto stationDto = new StationDto(schedule.getIdStation().getId(), schedule.getIdStation().getStationName(), schedule.getIdStation().getStationAbbr());
            ScheduleDto scheduleDto = new ScheduleDto(schedule.getId(), stationDto, trainDto, schedule.getTime());
            scheduleDtos.add(scheduleDto);
        }

        return scheduleDtos;

    }
}
