package server.services;

import common.command.TicketSell;
import common.dto.ErrorDto;
import common.dto.SuccessDto;
import server.dao.daoImpl.StationDaoImpl;
import server.dao.daoImpl.TicketDaoImpl;
import server.dao.daoImpl.TrainDaoImpl;
import server.dao.daoImpl.UserDaoImpl;
import server.models.Ticket;
import server.models.Train;
import server.models.User;

import java.sql.Time;
import java.util.Date;
import java.util.List;

import static common.DateUtils.utilDateToSqlTime;


/**
 * Created by Natali on 28/03/14.
 */
public class TicketSellProcessor {

    private TicketSell inputCommand;

    public TicketSellProcessor(TicketSell inputCommand) {
        this.inputCommand = inputCommand;
    }

    public Object process() {

        Date currentTime = new Date();
        Time time = inputCommand.getTimeOfTrain();
        time = new Time(time.getTime() - 60 * 10 * 1000);

        if (utilDateToSqlTime(currentTime).before(time)) {

            // find bought tickets by date and train, compare number of sold to id_train - num places
            TicketDaoImpl ticketDao = new TicketDaoImpl();
            List<Ticket> tickets = ticketDao.findTicketsPerTrainDate(inputCommand.getTrainId(), inputCommand.getDateDep());

            TrainDaoImpl trainDao = new TrainDaoImpl();
            Train train = trainDao.findByID(inputCommand.getTrainId());

            if (tickets.size() != train.getNumPlaces()) {
                // find all users with all data (FIO)
                UserDaoImpl userDao = new UserDaoImpl();
                List<User> users = userDao.findUsersWithFIO(inputCommand.getName(), inputCommand.getSurname(), inputCommand.getDateOfBirth());

                for (Ticket ticket : tickets) {
                    for (User user : users) {
                        if (ticket.getIdUser().getId() == user.getId()) {
                            return new ErrorDto("This user already have a ticket.");
                        }
                    }
                }

                //update User
                long userId = inputCommand.getUserId();
                User user = userDao.findByID(userId);
                if (user.getDateOfBirth() == null) {
                    user.setName(inputCommand.getName());
                    user.setSurname(inputCommand.getSurname());
                    user.setDateOfBirth(inputCommand.getDateOfBirth());
                    userDao.update(user);
                }
                // add ticket

                Ticket ticket = new Ticket();

                ticket.setDateOfDep(inputCommand.getDatePurchase());
                ticket.setDateOfPurchase(inputCommand.getDatePurchase());
                ticket.setConfirmation(false);
                ticket.setIdTrain(train);

                StationDaoImpl stationDao = new StationDaoImpl();
                ticket.setDepStation(stationDao.findByID(inputCommand.getStatDepId()));
                ticket.setArrivStation(stationDao.findByID(inputCommand.getStatArrId()));
                ticket.setIdUser(user);
                ticket.setStatus(true);

                ticketDao.create(ticket);


            } else {
                return new ErrorDto("No tickets left for this train.");
            }
        } else {
            return new ErrorDto("Too late for buying a ticket.");
        }

        return new SuccessDto();
    }


}