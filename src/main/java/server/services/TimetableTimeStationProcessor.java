package server.services;

import common.command.TimetableTimeStations;
import common.dto.ScheduleDto;
import common.dto.StationDto;
import common.dto.TrainDto;
import server.dao.daoImpl.ScheduleDaoImpl;
import server.models.Schedule;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Natali on 28/03/14.
 */
public class TimetableTimeStationProcessor {


    private TimetableTimeStations inputCommand;

    public TimetableTimeStationProcessor(TimetableTimeStations inputCommand) {
        this.inputCommand = inputCommand;
    }

    public Object process() {

        ScheduleDaoImpl scheduleDao = new ScheduleDaoImpl();

        List<ScheduleDto> scheduleDtos = new ArrayList<ScheduleDto>();

        if (inputCommand.isDeparture()) {
            Time time = inputCommand.getTime();
            Time depTime = new Time(time.getTime() - 60 * 15 * 1000);

            //System.out.println("depTime  " + depTime);

            List<Schedule> schedules1 = scheduleDao.findScheduleTimeStations(inputCommand.getStatDepId(), depTime);
            List<Schedule> schedules2 = scheduleDao.findSchedulePerStation(inputCommand.getStatArrId());

//            List<Schedule> schedules1 = scheduleDao.findScheduleTimeStations((long)1, depTime);
//            List<Schedule> schedules2 = scheduleDao.findSchedulePerStation((long)2);

            System.out.println("schedule1 " + schedules1.toString());
            System.out.println("schedule2 " + schedules2.toString());
            List<Schedule> result = getOverlapSchedules(schedules1, schedules2);

            for (Schedule schedule : result) {
                //System.out.println("result in command - "+ schedule.toString());

                TrainDto trainDto = new TrainDto(schedule.getIdTrain().getId(), schedule.getIdTrain().getTrainNumber(), schedule.getIdTrain().getNumPlaces());
                //System.out.println("train - " + trainDto.toString());
                StationDto stationDto = new StationDto(schedule.getIdStation().getId(), schedule.getIdStation().getStationName(), schedule.getIdStation().getStationAbbr());
                //System.out.println("station - " + stationDto.toString());
                ScheduleDto scheduleDto = new ScheduleDto(schedule.getId(), stationDto, trainDto, schedule.getTime());
                //System.out.println("scheduke - " + scheduleDto.toString());
                scheduleDtos.add(scheduleDto);

            }

            return scheduleDtos;


        } else {

            Time time = inputCommand.getTime();
            Time arrivalTime = new Time(time.getTime() + 60 * 15 * 1000);
            List<Schedule> schedules1 = scheduleDao.findScheduleTimeStations(inputCommand.getStatArrId(), arrivalTime);
            List<Schedule> schedules2 = scheduleDao.findSchedulePerStation(inputCommand.getStatDepId());

            List<Schedule> result = getOverlapSchedules(schedules1, schedules2);

            for (Schedule schedule : result) {
                TrainDto trainDto = new TrainDto(schedule.getIdTrain().getId(), schedule.getIdTrain().getTrainNumber(), schedule.getIdTrain().getNumPlaces());
                StationDto stationDto = new StationDto(schedule.getIdStation().getId(), schedule.getIdStation().getStationName(), schedule.getIdStation().getStationAbbr());
                ScheduleDto scheduleDto = new ScheduleDto(schedule.getId(), stationDto, trainDto, schedule.getTime());

                scheduleDtos.add(scheduleDto);
            }

            return scheduleDtos;

        }

    }

    private static List<Schedule> getOverlapSchedules(List<Schedule> schedules1, List<Schedule> schedules2) {
        List<Schedule> result = new ArrayList<Schedule>();
        for (Schedule schedule1 : schedules1) {
            for (Schedule schedule2 : schedules2) {
                if (schedule1.getIdTrain().getId() == schedule2.getIdTrain().getId()) {
                    result.add(schedule1);
                }
            }
        }
        return result;
    }

}

