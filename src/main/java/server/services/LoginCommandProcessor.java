package server.services;

import common.command.LoginCommand;
import common.dto.CredentialsDto;
import server.dao.daoImpl.UserDaoImpl;

/**
 * Created by Natali on 28/03/14.
 */
public class LoginCommandProcessor {

    private LoginCommand inputCommand;

    public LoginCommandProcessor(LoginCommand inputCommand) {
        this.inputCommand = inputCommand;
    }

    public Object process() {

        UserDaoImpl userDao = new UserDaoImpl();


        return new CredentialsDto(userDao.loginWithPassword(inputCommand.getLogin(), inputCommand.getPassword()));

        //true/false - find this login - find id - find passw - if match = OK


        //return new Object();
    }
}
