package server.services;

import common.command.TicketsTrainDate;
import common.dto.StationDto;
import common.dto.TicketDto;
import common.dto.TrainDto;
import common.dto.UserDto;
import server.dao.daoImpl.TicketDaoImpl;
import server.models.Ticket;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Natali on 28/03/14.
 */
public class TicketsTrainDateProcessor {


    private TicketsTrainDate inputCommand;

    public TicketsTrainDateProcessor(TicketsTrainDate inputCommand) {
        this.inputCommand = inputCommand;
    }


    public Object process() {

        TicketDaoImpl ticketsDao = new TicketDaoImpl();

        List<TicketDto> ticketDtos = new ArrayList<TicketDto>();

        List<Ticket> tickets = ticketsDao.findTicketsPerTrainDate(inputCommand.getTrainId(), inputCommand.getDate());

        for (Ticket ticket : tickets) {

            UserDto userDto = new UserDto(ticket.getIdUser().getId(), (int) ticket.getIdUser().getRole().getId());
            userDto.setName(ticket.getIdUser().getName());
            userDto.setSurname(ticket.getIdUser().getSurname());
            userDto.setDateOfBirth(ticket.getIdUser().getDateOfBirth());

            TrainDto trainDto = new TrainDto(ticket.getIdTrain().getId(), ticket.getIdTrain().getTrainNumber(), ticket.getIdTrain().getNumPlaces());
            StationDto stationDtoDep = new StationDto(ticket.getDepStation().getId(), ticket.getDepStation().getStationName(), ticket.getDepStation().getStationAbbr());
            StationDto stationDtoArriv = new StationDto(ticket.getArrivStation().getId(), ticket.getArrivStation().getStationName(), ticket.getArrivStation().getStationAbbr());

            TicketDto ticketDto = new TicketDto(userDto, ticket.getDateOfPurchase(), ticket.getDateOfDep(), trainDto, stationDtoDep, stationDtoArriv);
            ticketDtos.add(ticketDto);
        }


        return ticketDtos;

    }


}
