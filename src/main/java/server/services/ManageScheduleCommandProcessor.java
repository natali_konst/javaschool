package server.services;

import common.command.ManageScheduleCommand;
import common.dto.SuccessDto;
import server.dao.daoImpl.ScheduleDaoImpl;
import server.dao.daoImpl.StationDaoImpl;
import server.dao.daoImpl.TrainDaoImpl;
import server.models.Schedule;

/**
 * Created by Natali on 28/03/14.
 */
public class ManageScheduleCommandProcessor {

    private ManageScheduleCommand inputCommand;

    public ManageScheduleCommandProcessor(ManageScheduleCommand inputCommand) {
        this.inputCommand = inputCommand;
    }

    public Object process() {

        ScheduleDaoImpl scheduleDao = new ScheduleDaoImpl();
        TrainDaoImpl trainDao = new TrainDaoImpl();
        StationDaoImpl stationDao = new StationDaoImpl();

        switch (inputCommand.getAction()) {
            case 1: //add
            {
                Schedule schedule = new Schedule();

                schedule.setIdTrain(trainDao.findByID((inputCommand.getTrainId()).getTrainId()));

                schedule.setIdStation(stationDao.findByID((inputCommand.getStationId()).getStationId()));
                schedule.setTime(inputCommand.getTime());
                scheduleDao.create(schedule);
                break;
            }
            case 2: //delete
            {
                Schedule schedule = new Schedule();
                schedule.setId(inputCommand.getScheduleId());
                schedule.setIdTrain(trainDao.findByID((inputCommand.getTrainId()).getTrainId()));
                schedule.setIdStation(stationDao.findByID((inputCommand.getStationId()).getStationId()));
                schedule.setTime(inputCommand.getTime());
                scheduleDao.delete(schedule);
                break;
            }
            case 3: //update
            {
                Schedule schedule = new Schedule();
                schedule.setId(inputCommand.getScheduleId());
                schedule.setIdTrain(trainDao.findByID((inputCommand.getTrainId()).getTrainId()));
                schedule.setIdStation(stationDao.findByID((inputCommand.getStationId()).getStationId()));
                schedule.setTime(inputCommand.getTime());
                scheduleDao.update(schedule);
                break;
            }
        }

        return new SuccessDto();

    }
}
