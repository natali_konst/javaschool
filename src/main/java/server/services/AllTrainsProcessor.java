package server.services;

import common.command.AllTrains;
import common.dto.TrainDto;
import server.dao.daoImpl.TrainDaoImpl;
import server.models.Train;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Natali on 28/03/14.
 */
public class AllTrainsProcessor {


    private AllTrains inputCommand;

    public AllTrainsProcessor(AllTrains inputCommand) {
        this.inputCommand = inputCommand;
    }

    public Object process() {

        TrainDaoImpl trainDao = new TrainDaoImpl();
        List<TrainDto> trainDtos = new ArrayList<TrainDto>();

        List<Train> trains = trainDao.findAll();

        for (Train train : trains) {
            TrainDto trainDto = new TrainDto(train.getId(), train.getTrainNumber(), train.getNumPlaces());
            trainDtos.add(trainDto);
        }


        return trainDtos;

    }

}
