package server.services;

import common.command.AllStations;
import common.dto.StationDto;
import server.dao.daoImpl.StationDaoImpl;
import server.models.Station;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Natali on 28/03/14.
 */
public class AllStationsProcessor {


    private AllStations inputCommand;

    public AllStationsProcessor(AllStations inputCommand) {
        this.inputCommand = inputCommand;
    }

    public Object process() {

        StationDaoImpl stationDao = new StationDaoImpl();
        List<StationDto> stationDtos = new ArrayList<StationDto>();

        List<Station> stations = stationDao.findAll();


        for (Station station : stations) {
            StationDto stationDto = new StationDto(station.getId(), station.getStationName(), station.getStationAbbr());
            System.out.println("check");
            stationDtos.add(stationDto);
        }

        return stationDtos;

    }


}
