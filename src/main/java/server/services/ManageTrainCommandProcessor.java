package server.services;

import common.command.ManageTrainCommand;
import common.dto.SuccessDto;
import server.dao.daoImpl.TrainDaoImpl;
import server.models.Train;

/**
 * Created by Natali on 28/03/14.
 */
public class ManageTrainCommandProcessor {

    private ManageTrainCommand inputCommand;

    public ManageTrainCommandProcessor(ManageTrainCommand inputCommand) {
        this.inputCommand = inputCommand;
    }

    public Object process() {

        TrainDaoImpl trainDao = new TrainDaoImpl();

        switch (inputCommand.getAction()) {
            case 1: //add
            {
                Train train = new Train();
                train.setNumPlaces(inputCommand.getNumberPlaces());
                train.setTrainNumber(inputCommand.getTrainNumber());
                train.setWeekendTrain(false);
                trainDao.create(train);
                break;
            }
            case 2: //delete
            {
                Train train = new Train();
                train.setId(inputCommand.getTrainId());
                train.setNumPlaces(inputCommand.getNumberPlaces());
                train.setTrainNumber(inputCommand.getTrainNumber());
                train.setWeekendTrain(false);
                trainDao.delete(train);
                break;
            }
            case 3: //update
            {
                Train train = new Train();
                train.setId(inputCommand.getTrainId());
                train.setNumPlaces(inputCommand.getNumberPlaces());
                train.setTrainNumber(inputCommand.getTrainNumber());
                train.setWeekendTrain(false);
                trainDao.update(train);
                break;
            }
        }

        return new SuccessDto();

    }
}
