package server.services;

import common.Command;
import common.command.*;
import common.dto.ErrorDto;

/**
 * Created by Natali on 28/03/14.
 */
public class CommandProcessor {

    private Command command;

    public CommandProcessor(Command command) {
        this.command = command;
    }

    public Object process() {

        Object output;

        if (command instanceof LoginCommand) {
            return new LoginCommandProcessor((LoginCommand) command).process();
        }

        if (command instanceof AllStations) {
            return new AllStationsProcessor((AllStations) command).process();
        }

        if (command instanceof AllTrains) {
            return new AllTrainsProcessor((AllTrains) command).process();
        }

        if (command instanceof GetStationTimetable) {
            return new GetStationTimetableProcessor((GetStationTimetable) command).process();
        }

        if (command instanceof ManageScheduleCommand) {
            return new ManageScheduleCommandProcessor((ManageScheduleCommand) command).process();
        }

        if (command instanceof ManageStationCommand) {
            return new ManageStationCommandProcessor((ManageStationCommand) command).process();
        }

        if (command instanceof ManageTrainCommand) {
            return new ManageTrainCommandProcessor((ManageTrainCommand) command).process();
        }

//        if (command instanceof ManageUserCommand) {
//            return new ManageUserCommandProcessor((ManageUserCommand)command).process(); }
//
//        if (command instanceof RegistrationCommand) {
//            return new RegistrationCommandProcessor((RegistrationCommand)command).process(); }
//
        if (command instanceof TicketSell) {
            return new TicketSellProcessor((TicketSell) command).process();
        }

        if (command instanceof TicketsTrainDate) {
            return new TicketsTrainDateProcessor((TicketsTrainDate) command).process();
        }

        if (command instanceof TimetableTimeStations) {
            return new TimetableTimeStationProcessor((TimetableTimeStations) command).process();
        }


        return new ErrorDto("Unknown command encountered.");
    }

}
