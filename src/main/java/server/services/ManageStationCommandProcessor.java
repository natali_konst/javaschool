package server.services;

import common.command.ManageStationCommand;
import common.dto.SuccessDto;
import server.dao.daoImpl.StationDaoImpl;
import server.models.Station;

/**
 * Created by Natali on 28/03/14.
 */
public class ManageStationCommandProcessor {

    private ManageStationCommand inputCommand;

    public ManageStationCommandProcessor(ManageStationCommand inputCommand) {
        this.inputCommand = inputCommand;
    }

    public Object process() {

        StationDaoImpl stationDao = new StationDaoImpl();

        switch (inputCommand.getAction()) {
            case 1: //add
            {
                Station station = new Station();
                station.setStationAbbr(inputCommand.getStationAbbr());
                station.setStationName(inputCommand.getStationName());
                stationDao.create(station);
                break;
            }
            case 2: //delete
            {
                Station station = new Station();
                station.setId(inputCommand.getStationId());
                station.setStationAbbr(inputCommand.getStationAbbr());
                station.setStationName(inputCommand.getStationName());
                stationDao.delete(station);
                break;
            }
            case 3: //update
            {
                Station station = new Station();
                station.setId(inputCommand.getStationId());
                station.setStationAbbr(inputCommand.getStationAbbr());
                station.setStationName(inputCommand.getStationName());
                stationDao.update(station);
                break;
            }
        }

        return new SuccessDto();

    }
}
