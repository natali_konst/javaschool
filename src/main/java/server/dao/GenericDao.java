package server.dao;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Natali on 25/03/14.
 */
public interface GenericDao<T, PK extends Serializable> {

    T create(T value);

    void delete(T t);

    T findByID(PK id);

    T update(T value);

    //void deleteObject (PK key);
    List<T> findAll();


}
