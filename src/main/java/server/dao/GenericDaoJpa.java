package server.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Created by Natali on 26/03/14.
 */
public abstract class GenericDaoJpa<T, PK extends Serializable> implements GenericDao<T, PK> {
    protected Class<T> entityClass;


    private static EntityManagerFactory emf = Persistence
            .createEntityManagerFactory("myapp");

    @PersistenceContext
    protected EntityManager entityManager = emf.createEntityManager();


    public GenericDaoJpa() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass()
                .getGenericSuperclass();
        this.entityClass = (Class<T>) genericSuperclass
                .getActualTypeArguments()[0];
    }

    @Override
    public T create(T t) {
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(t);
        this.entityManager.getTransaction().commit();

        return t;
    }

    @Override
    public void delete(T t) {
        this.entityManager.getTransaction().begin();
        t = this.entityManager.merge(t);    //todo think of deleting entries that are FK somewhere
        this.entityManager.remove(t);
        this.entityManager.getTransaction().commit();
    }

    @Override
    public T findByID(PK id) {
        return this.entityManager.find(entityClass, id);
    }

    @Override
    public T update(T t) {
        this.entityManager.getTransaction().begin();
        this.entityManager.merge(t);
        this.entityManager.getTransaction().commit();
        return t;
    }

    @Override
    public List<T> findAll() {
        return this.entityManager.createQuery("FROM " + entityClass.getName()).getResultList();

        //Query query = em.createQuery("SELECT e FROM Professor e");

    }


}
