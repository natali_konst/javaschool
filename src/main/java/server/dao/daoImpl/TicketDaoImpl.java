package server.dao.daoImpl;

import server.dao.GenericDaoJpa;
import server.models.Ticket;

import javax.persistence.Query;
import java.util.Date;
import java.util.List;

import static common.DateUtils.utilDateToSqlDate;

/**
 * Created by Natali on 26/03/14.
 */
public class TicketDaoImpl extends GenericDaoJpa<Ticket, Long> implements TicketDao {

    public List<Ticket> findTicketsSubmittedSince(Date date) {
        Query q = entityManager.createQuery(
                "SELECT e FROM " + entityClass.getName() + " e WHERE dateOfPurchase >= :date_since");
        q.setParameter("date_since", date);
        return (List<Ticket>) q.getResultList();
    }

    public List<Ticket> findTicketsPerTrainDate(Long trainId, Date date) {

//        Calendar cal = Calendar.getInstance();
//        cal.setTime(date);
//        String dateString = ;

        java.sql.Date dateSql = utilDateToSqlDate(date);


        Query q = entityManager.createQuery(
                "SELECT e FROM " + entityClass.getName() + " e WHERE ((idTrain.id = :train_id) and (dateOfDep = :dep_date))");
        q.setParameter("dep_date", dateSql).setParameter("train_id", trainId);

        return (List<Ticket>) q.getResultList();

    }


}
