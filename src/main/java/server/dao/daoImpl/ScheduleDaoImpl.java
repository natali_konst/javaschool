package server.dao.daoImpl;

import server.dao.GenericDaoJpa;
import server.models.Schedule;

import javax.persistence.Query;
import java.sql.Time;
import java.util.List;

/**
 * Created by Natali on 26/03/14.
 */
public class ScheduleDaoImpl extends GenericDaoJpa<Schedule, Long> implements ScheduleDao {

    public List<Schedule> findSchedulePerTrain(long id)

    {
        Query q = entityManager.createQuery(
                "SELECT e FROM " + entityClass.getName() + " e WHERE idTrain.id = :trainId");
        q.setParameter("trainId", id);

        return (List<Schedule>) q.getResultList();

    }

    public List<Schedule> findSchedulePerStation(long id)

    {
        Query q = entityManager.createQuery(
                "SELECT e FROM " + entityClass.getName() + " e WHERE idStation.id = :stationId");
        q.setParameter("stationId", id); //TODO have everywhere long and not Long

        return (List<Schedule>) q.getResultList();

    }

    public List<Schedule> findScheduleTimeStations(long stationId, Time time) {
        Query q = entityManager.createQuery(
                "SELECT e FROM " + entityClass.getName() + " e WHERE ((idStation.id = :station_id) and (time >= :time))");
        q.setParameter("time", time).setParameter("station_id", stationId);

        return (List<Schedule>) q.getResultList();

    }

//    public List<Schedule> findScheduleTimeTwoStations(Long stationOneId, Long stationTwoId, Time time)
//    {
//        Query q = entityManager.createQuery(
//                "SELECT e FROM " + entityClass.getName() + " e WHERE (idStation.id = :station1_id and time = :time") AND ();
//        q.setParameter("time", time).setParameter("station_id", stationId);
//
//        return (List<Schedule>) q.getResultList();
//    }
//

}
