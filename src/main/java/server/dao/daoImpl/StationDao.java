package server.dao.daoImpl;

import server.dao.GenericDao;
import server.models.Station;

/**
 * Created by Natali on 26/03/14.
 */
public interface StationDao extends GenericDao<Station, Long> {

}

