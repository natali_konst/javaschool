package server.dao.daoImpl;

import server.dao.GenericDao;
import server.models.Role;

/**
 * Created by Natali on 26/03/14.
 */
public interface RoleDao extends GenericDao<Role, Long> {

}

