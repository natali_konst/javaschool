package server.dao.daoImpl;

import server.dao.GenericDao;
import server.models.Ticket;

import java.util.Date;
import java.util.List;

/**
 * Created by Natali on 26/03/14.
 */
public interface TicketDao extends GenericDao<Ticket, Long> {

    List<Ticket> findTicketsSubmittedSince(Date date);

    List<Ticket> findTicketsPerTrainDate(Long trainId, Date date);

}
