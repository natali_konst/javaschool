package server.dao.daoImpl;

import server.dao.GenericDaoJpa;
import server.models.Role;
import server.models.User;

import javax.persistence.Query;
import java.util.Date;
import java.util.List;

/**
 * Created by Natali on 26/03/14.
 */
public class UserDaoImpl extends GenericDaoJpa<User, Long> implements UserDao {

    public List<User> findUsersWithFIO(String name, String surname, Date dateOfBirth)

    {
        Query q = entityManager.createQuery(
                "SELECT e FROM " + entityClass.getName() + " e WHERE (name = :user_name and surname = :user_surname and dateOfBirth = :date_birth)");
        q.setParameter("user_name", name).setParameter("user_surname", surname).setParameter("date_birth", dateOfBirth);

        return (List<User>) q.getResultList();
    }

    public Role loginWithPassword(String login, String password) {
        Query q = entityManager.createQuery(
                "SELECT e FROM " + entityClass.getName() + " e WHERE (login = :user_login and password = :user_passw)");
        q.setParameter("user_login", login).setParameter("user_passw", password);

        List<User> users = q.getResultList();
        if (users.size() == 1) {
            Role role = users.get(0).getRole();
            return role;
        }
        return null;
    }
}
