package server.dao.daoImpl;

import server.dao.GenericDao;
import server.models.Train;

/**
 * Created by Natali on 26/03/14.
 */
public interface TrainDao extends GenericDao<Train, Long> {

}

