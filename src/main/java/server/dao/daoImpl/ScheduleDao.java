package server.dao.daoImpl;

import server.dao.GenericDao;
import server.models.Schedule;

import java.sql.Time;
import java.util.List;

/**
 * Created by Natali on 26/03/14.
 */
public interface ScheduleDao extends GenericDao<Schedule, Long> {

    List<Schedule> findSchedulePerTrain(long id);

    List<Schedule> findSchedulePerStation(long id);

    List<Schedule> findScheduleTimeStations(long stationId, Time time);

//    List<Schedule> findScheduleTimeTwoStations(Long stationOneId, Long stationTwoId, Time time);

}

