package server.dao.daoImpl;

import server.dao.GenericDao;
import server.models.Role;
import server.models.User;

import java.util.Date;
import java.util.List;

/**
 * Created by Natali on 26/03/14.
 */
public interface UserDao extends GenericDao<User, Long> {

    List<User> findUsersWithFIO(String name, String surname, Date dateOfBirth);

    Role loginWithPassword(String login, String password);

}

