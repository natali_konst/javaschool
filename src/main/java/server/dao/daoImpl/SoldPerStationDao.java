package server.dao.daoImpl;

import server.dao.GenericDao;
import server.models.SoldPerStation;

/**
 * Created by Natali on 26/03/14.
 */
public interface SoldPerStationDao extends GenericDao<SoldPerStation, Long> {

}

