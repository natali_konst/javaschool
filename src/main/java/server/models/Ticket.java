package server.models;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Natali on 24/03/14.
 */
@Entity
@Table(name = "Tickets")
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "DateOfPurchase", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dateOfPurchase;    //????
    @Column(name = "Confrimation", nullable = false)
    private Boolean confirmation;
    @Column(name = "DateOfDep", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dateOfDep;    //????
    @ManyToOne(optional = false)
    @JoinColumn(name = "idTrain", referencedColumnName = "id")
    private Train idTrain;
    @ManyToOne(optional = false)
    @JoinColumn(name = "DepStation", referencedColumnName = "id")
    private Station depStation;
    @ManyToOne(optional = false)
    @JoinColumn(name = "ArrivStation", referencedColumnName = "id")
    private Station arrivStation;
    @ManyToOne(optional = false)
    @JoinColumn(name = "idUser", referencedColumnName = "id")
    private User idUser;
    @Column(name = "Status", nullable = false)
    private Boolean status;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDateOfPurchase() {
        return dateOfPurchase;
    }

    public void setDateOfPurchase(Date dateOfPurchase) {
        this.dateOfPurchase = dateOfPurchase;
    }

    public Boolean getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(Boolean confirmation) {
        this.confirmation = confirmation;
    }

    public Date getDateOfDep() {
        return dateOfDep;
    }

    public void setDateOfDep(Date dateOfDep) {
        this.dateOfDep = dateOfDep;
    }

    public Train getIdTrain() {
        return idTrain;
    }

    public void setIdTrain(Train idTrain) {
        this.idTrain = idTrain;
    }

    public Station getDepStation() {
        return depStation;
    }

    public void setDepStation(Station depStation) {
        this.depStation = depStation;
    }

    public Station getArrivStation() {
        return arrivStation;
    }

    public void setArrivStation(Station arrivStation) {
        this.arrivStation = arrivStation;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public User getIdUser() {
        return idUser;
    }

    public void setIdUser(User idUser) {
        this.idUser = idUser;
    }
}
