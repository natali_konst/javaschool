package server.models;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Natali on 24/03/14.
 */
@Entity
@Table(name = "Sold_perStation")
public class SoldPerStation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "Sold", nullable = false)
    private long sold;
    @Column(name = "Date", nullable = false)
    private Date date;
    @ManyToOne(optional = false)
    @JoinColumn(name = "Schedule_id", referencedColumnName = "id")
    private Schedule idSchedule;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getSold() {
        return sold;
    }

    public void setSold(long sold) {
        this.sold = sold;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Schedule getIdSchedule() {
        return idSchedule;
    }

    public void setIdSchedule(Schedule idSchedule) {
        this.idSchedule = idSchedule;
    }


}
