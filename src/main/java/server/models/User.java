package server.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"Login"}), name = "User")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "Login", nullable = false, length = 20)
    private String login;
    @Column(name = "Password", nullable = false, length = 15)
    private String password;
    @Column(name = "Email")
    private String email;
    @ManyToOne(optional = false)
    @JoinColumn(name = "idRole", referencedColumnName = "id")
    private Role role;
    @Column(name = "Name", length = 45)
    private String name;
    @Column(name = "Surname", length = 45)
    private String surname;
    @Column(name = "DateOfBirth")
    private Date dateOfBirth;
//
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "Tickets")   ///????
//    private Collection<Tickets> ticketsCollection;

    // getter and setter

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

}
