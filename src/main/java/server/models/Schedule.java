package server.models;

import javax.persistence.*;
import java.sql.Time;

/**
 * Created by Natali on 24/03/14.
 */
@Entity
@Table(name = "Schedule")
public class Schedule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "Time", nullable = false)
    private Time time;
    @ManyToOne(optional = false)
    @JoinColumn(name = "idStation", referencedColumnName = "id")
    private Station idStation;
    @ManyToOne(optional = false)
    @JoinColumn(name = "Train_id", referencedColumnName = "id")
    private Train idTrain;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public Station getIdStation() {
        return idStation;
    }

    public void setIdStation(Station idStation) {
        this.idStation = idStation;
    }

    public Train getIdTrain() {
        return idTrain;
    }

    public void setIdTrain(Train idTrain) {
        this.idTrain = idTrain;
    }

}
