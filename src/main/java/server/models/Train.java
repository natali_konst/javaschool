package server.models;

import javax.persistence.*;

/**
 * Created by Natali on 24/03/14.
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"TrainNumber"}), name = "Train")
public class Train {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "NumPlaces", nullable = false)
    private long numPlaces;
    @Column(name = "TrainNumber", nullable = false, length = 10)
    private String trainNumber;
    @Column(name = "WeekendTrain")
    private Boolean weekendTrain;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public long getNumPlaces() {
        return numPlaces;
    }

    public void setNumPlaces(long numPlaces) {
        this.numPlaces = numPlaces;
    }

    public Boolean getWeekendTrain() {
        return weekendTrain;
    }

    public void setWeekendTrain(Boolean weekendTrain) {
        this.weekendTrain = weekendTrain;
    }


}
