package server.models;

import javax.persistence.*;


/**
 * Created by Natali on 24/03/14.
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"RoleName"}), name = "Role")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "RoleName", nullable = false, length = 45)
    private String roleName;

//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "User")
//    private Collection<User> userCollection;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

}
