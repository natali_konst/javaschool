package server.models;

import javax.persistence.*;

/**
 * Created by Natali on 24/03/14.
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"StationAbbr", "StationName"}), name = "Station")
public class Station {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "StationAbbr", nullable = false, length = 10)
    private String stationAbbr;
    @Column(name = "StationName", nullable = false, length = 45)
    private String stationName;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStationAbbr() {
        return stationAbbr;
    }

    public void setStationAbbr(String stationAbbr) {
        this.stationAbbr = stationAbbr;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }
}
