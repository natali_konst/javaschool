package server;

/**
 * Created by Natali on 28/03/14.
 */
public class BusinessException extends Exception {
    BusinessException() {
    }

    BusinessException(String msg) {
        super(msg);
    }

}

