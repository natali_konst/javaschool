import client.Client;
import common.DateUtils;
import common.command.*;
import common.dto.*;
import server.Server;
import server.dao.daoImpl.RoleDaoImpl;
import server.dao.daoImpl.UserDaoImpl;
import server.models.Role;
import server.models.User;
import server.services.CommandProcessor;

import java.io.IOException;
import java.sql.Time;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import static common.DateUtils.utilDateToSqlTime;

/**
 * Created by Natali on 25/03/14.
 */
public class Test {

    final static int PORTNUMBER = 9990;
    final static int NUMBERCON = 3;


    public static void testServer() {


        Server socketServer = new Server(PORTNUMBER);
        socketServer.run();

    }

//
//    public static void testClient() {
//
//        Client client = new Client("localhost", 9990);
//        try {
//            client.connect();
//            client.communicateWithServer();
//
//        } catch (IOException e) {
//            System.err.println("Cannot establish connection. Server may not be up." + e.getMessage());
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//
//    }

    public static void testLogin() {
        LoginCommand loginCommand = new LoginCommand("testing1", "testing1");
        CommandProcessor commandProcessor = new CommandProcessor(loginCommand);
        CredentialsDto credentialsDto = (CredentialsDto) commandProcessor.process();
        System.out.println(credentialsDto.getRole());

        LoginCommand loginCommand2 = new LoginCommand("testing1", "test");
        CommandProcessor commandProcessor2 = new CommandProcessor(loginCommand2);
        CredentialsDto credentialsDto2 = (CredentialsDto) commandProcessor2.process();
        System.out.println(credentialsDto2.getRole());

    }

    public static void testHibernate() {
        RoleDaoImpl roleDao = new RoleDaoImpl();

        Role r = new Role();
        r.setId(15);
        r.setRoleName("Test11");
        roleDao.update(r);
        //roleDao.create(r);

        roleDao.findAll();
        for (Role role : roleDao.findAll()) {
            System.out.println(role.getRoleName());
        }

        UserDaoImpl userDao = new UserDaoImpl();
        User user = new User();
        user.setDateOfBirth(new Date());
        user.setLogin("testing4");
        user.setPassword("testing3");

        Role role = roleDao.findByID(Long.parseLong("5"));
        user.setRole(role);

        userDao.create(user);

    }

    public static void testAllStations() {
        AllStations allStations = new AllStations();
        CommandProcessor commandProcessor = new CommandProcessor(allStations);
        List<StationDto> stationDtos = (List<StationDto>) commandProcessor.process();
        for (StationDto stationDto : stationDtos) {
            System.out.println(stationDto.getStationId() + stationDto.getStationAbbr() + stationDto.getStationName());
        }

    }

    public static void testAllTrains() throws IOException, ClassNotFoundException {

        final Client client = new Client("localhost", 9900);
        client.connect();
        AllTrains allTrains = new AllTrains();
        Object trainDtos = client.communicateWithServer(allTrains);
        //CommandProcessor commandProcessor = new CommandProcessor(allTrains);
        //List<TrainDto> trainDtos = (List<TrainDto>) commandProcessor.process();
        for (TrainDto trainDto : (List<TrainDto>) trainDtos) {
            System.out.println(trainDto.getTrainId() + trainDto.getTrainNumber() + trainDto.getNumberPlaces());
        }

    }

    public static void testStationTimetable() {

        GetStationTimetable getStationTimetable = new GetStationTimetable((long) (1));
        CommandProcessor commandProcessor = new CommandProcessor(getStationTimetable);
        List<ScheduleDto> scheduleDtos = (List<ScheduleDto>) commandProcessor.process();
        for (ScheduleDto scheduleDto : scheduleDtos) {
            System.out.println(scheduleDto.getScheduleId() + scheduleDto.getStationId().getStationName() + scheduleDto.getTime().toString() + scheduleDto.getTrainId().getTrainNumber());
        }
    }

    public static void testStationManagement() {

        ManageStationCommand manageStationCommand1 = new ManageStationCommand(1, "test1", "test1");
        CommandProcessor commandProcessor = new CommandProcessor(manageStationCommand1);
        Object result = commandProcessor.process();
        if (result instanceof SuccessDto) System.out.println("Added");

        ManageStationCommand manageStationCommand2 = new ManageStationCommand(2, (long) 8, "test", "test");
        CommandProcessor commandProcessor2 = new CommandProcessor(manageStationCommand2);
        Object result2 = commandProcessor2.process();
        if (result2 instanceof SuccessDto) System.out.println("Deleted");

        ManageStationCommand manageStationCommand3 = new ManageStationCommand(3, (long) 4, "Новгород", "NVR");
        CommandProcessor commandProcessor3 = new CommandProcessor(manageStationCommand3);
        Object result3 = commandProcessor3.process();
        if (result3 instanceof SuccessDto) System.out.println("Updated");

    }

    public static void testTicketsPerDate() {

        try {

            Date date = DateUtils.fromStringToDate("2013-05-16");
            System.out.println(date.toString());
            TicketsTrainDate ticketsTrainDate = new TicketsTrainDate(date, (long) 1);
            CommandProcessor commandProcessor = new CommandProcessor(ticketsTrainDate);
            List<TicketDto> ticketDtos = (List<TicketDto>) commandProcessor.process();


            for (TicketDto ticketDto : ticketDtos) {
                System.out.println(ticketDto.getUserId().getName());
            }


        } catch (ParseException e) {
            System.out.println("Incorrect date format. Use yyyy-MM-dd.");
            e.printStackTrace();
        }

    }

    public static void testScheduleTimeStations() throws IOException, ClassNotFoundException {

        final Client client = new Client("localhost", 9900);
        client.connect();
        Time time = Time.valueOf("06:30:00");
        System.out.println(time.toString());
        TimetableTimeStations timetableTimeStations = new TimetableTimeStations(time, (long) 1, (long) 2, true);
//        CommandProcessor commandProcessor = new CommandProcessor(timetableTimeStations);
        //List<ScheduleDto> scheduleDtos = (List<ScheduleDto>) commandProcessor.process();
        Object scheduleDtos = client.communicateWithServer(timetableTimeStations);

        for (ScheduleDto scheduleDto : (List<ScheduleDto>) scheduleDtos) {
            System.out.println(scheduleDto.toString());
            //System.out.println(scheduleDto.getTime() + "  " + scheduleDto.getStationId() + " " + scheduleDto.getTrainId());
        }


    }

    public static void testTicketsSell() {

        try {
            Date date = DateUtils.fromStringToDate("2013-05-19");
            Time time = Time.valueOf("06:30:00");
            TicketSell ticketSell = new TicketSell();
            ticketSell.setTrainId((long) 1);
            ticketSell.setDateDep(date);
            ticketSell.setTimeOfTrain(time);
            ticketSell.setDatePurchase(date);
            ticketSell.setStatDepId((long) 1);
            ticketSell.setStatArrId((long) 2);

            ticketSell.setDateOfBirth(DateUtils.fromStringToDate("2014-03-27"));
            ticketSell.setName("Test");
            ticketSell.setSurname("Test");
            ticketSell.setUserId((long) 1);

            CommandProcessor commandProcessor = new CommandProcessor(ticketSell);
            Object result = commandProcessor.process();

            if (result instanceof SuccessDto) {
                System.out.println(" Everything without problems");
            }

            if (result instanceof ErrorDto) {
                System.out.println(((ErrorDto) result).getError());
            }


        } catch (ParseException e) {
            System.out.println("Incorrect date format. Use yyyy-MM-dd.");
            e.printStackTrace();
        }

    }

    public static void testTimeComparison() {
        Date currentTime = new Date();
        Time time = Time.valueOf("22:50:00");
        System.out.println(time.toString());
        time = new Time(time.getTime() - 60 * 10 * 1000);

        Time currentTimeConv = utilDateToSqlTime(currentTime);

        if (currentTimeConv.after(time)) {
            System.out.println("Too late");
        } else {
            System.out.println("All ok!");
        }

        System.out.println("current - " + currentTimeConv.toString());
        System.out.println("checked - " + time.toString());


    }


    public static void main(final String[] args) {
//


        try {
            testScheduleTimeStations();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

}
