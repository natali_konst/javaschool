package client;

import common.Command;
import common.command.LoginCommand;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Natali on 20/03/14.
 */
public class Client implements Runnable {
    private final ConcurrentLinkedQueue<Operation> myOps = new ConcurrentLinkedQueue<Operation>();


    private String hostname;
    private int port;

    Socket socketClient;
    private boolean active;

    ObjectInputStream ois;
    ObjectOutputStream oos;

    public Client(String hostname, int port) {
        this.hostname = hostname;
        this.port = port;
    }

    public boolean isActive() {
        return active;
    }

    public void execute(Command command, client.Handler<Object> onSuccess, client.Handler<Throwable> onFail) {
        myOps.add(new Operation(command, onSuccess, onFail));
    }

    public void login(String login, String password, client.Handler<Object> onOk, client.Handler<Throwable> onFail) {
        new Thread(this).start();
        myOps.add(new Operation(new LoginCommand(login, password), onOk, onFail));

//        myOps.add(new Operation(new LoginCommand(login, password), new Handler<Object>()
//        {
//            @Override
//            public void handle(Object result) {
//                if (result instanceof CredentialsDto) {
//                    onOk.run();
//                }
//                if (result instanceof ErrorDto) {
//                    onFail.run();
//                }
//            }
//        }, new Handler<Throwable>() {
//            @Override
//            public void handle(Throwable result) {
//                onFail.run();
//            }
//        }));

    }

    public void connect() throws IOException {
        System.out.println("Attempting to connect to " + hostname + ": " + port);
        socketClient = new Socket(hostname, port);
        System.out.println("Connection Established");
        oos = new ObjectOutputStream(socketClient.getOutputStream());
        ois = new ObjectInputStream(socketClient.getInputStream());
    }


    public void stopClient() {
        try {
            socketClient.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        active = false;
    }

    public Object communicateWithServer(Command command) throws IOException, ClassNotFoundException {


        try {

            oos.writeObject(command);
            oos.flush();

            return ois.readObject();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

//        finally {
//            try {
//                if (ois != null) ois.close();
//                if (oos != null) oos.close();
//            } catch (Exception e) {
//                throw new RuntimeException(e);
//            }
//        }
    }


    @Override
    public void run() {
        //Client client = new Client("localhost", port);

        try {
            connect();
            active = true;

            while (true) {
                Operation op = myOps.poll();

                try {
                    if (op != null) op.onSuccess.handle(communicateWithServer(op.myCommand));
                } catch (Throwable e) {
                    op.onFail.handle(e);
                }
            }
        } catch (IOException e) {
            System.out.println("No luck");
            e.printStackTrace();
        }

    }

    private static class Operation {
        private final Command myCommand;
        private final client.Handler<Object> onSuccess;
        private final client.Handler<Throwable> onFail;

        private Operation(Command command, client.Handler<Object> onSuccess, client.Handler<Throwable> onFail) {
            this.myCommand = command;
            this.onSuccess = onSuccess;
            this.onFail = onFail;
        }
    }


    public static void main(String arg[]) {
        //Creating a Client object
        Client client = new Client("localhost", 9900);
        try {
            //trying to establish connection to the server
            client.connect();
            //client.communicateWithServer();

        } catch (UnknownHostException e) {
            System.err.println("Host unknown. Cannot establish connection");
        } catch (IOException e) {
            System.err.println("Cannot establish connection. Server may not be up." + e.getMessage());
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
        }
    }


//
//    public void readResponse() throws IOException {
//        String userInput;
//        BufferedReader stdIn = new BufferedReader(new InputStreamReader(socketClient.getInputStream()));
//
//        System.out.println("Response from server:");
//        while ((userInput = stdIn.readLine()) != null) {
//            System.out.println(userInput);
//        }
//    }
//
//    public void sendMessage(String message) throws IOException {
//        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socketClient.getOutputStream()));
//        writer.write(message);
//        writer.flush();
//        //writer.close();
//    }

}