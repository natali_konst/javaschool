package client;

/**
 * Created by Natali on 30/03/14.
 */
public interface Handler<ResultT> {

    void handle(ResultT result);
}
