package client.ui.user;

import javax.swing.*;
import java.awt.event.*;

public class Timetable extends JDialog {
    private JPanel contentPane;
    public JButton buttonSearch;
    private JButton buttonCancel;
    public JComboBox comboTrain;
    public JComboBox comboStation;
    private JTabbedPane tabbedPane1;
    public JTextField timeDepField;
    public JTextField dateDepField;
    private JPanel errorField;
    public JLabel errorLabel;
    private JTextField dateArrField;

    public Timetable() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonSearch);

        buttonSearch.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
// add your code here
        dispose();
    }

    private void onCancel() {
// add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        Timetable dialog = new Timetable();
        dialog.setTitle("Timetable");
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
