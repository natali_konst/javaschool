package client.ui;

public class LoginListener {
    private String loginField;
    private String passwordField;

    public LoginListener() {
    }

    public String getLoginField() {
        return loginField;
    }

    public void setLoginField(final String loginField) {
        this.loginField = loginField;
    }

    public String getPasswordField() {
        return passwordField;
    }

    public void setPasswordField(final String passwordField) {
        this.passwordField = passwordField;
    }
}