package client.ui.editor;

import javax.swing.*;
import java.awt.event.*;

public class AddTrain extends JDialog {
    private JPanel contentPane;
    public JButton buttonOK;
    private JButton buttonCancel;

    public JTextField nameField;
    public JTextField numPlacesField;
    private JPanel Error;
    private JPanel errorField;
    public JLabel errorLabel;

    public AddTrain() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
//        backButton.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                //todo
//            }
//        });
    }

    private void onOK() {
// add your code here
        dispose();
    }

    private void onCancel() {
// add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        AddTrain dialog = new AddTrain();
        dialog.setTitle("Add Train");
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    public void setData(TrainListener data) {
        nameField.setText(data.getTrainName());
        numPlacesField.setText(data.getTrainNumPlaces());
    }

    public void getData(TrainListener data) {
        data.setTrainName(nameField.getText());
        data.setTrainNumPlaces(numPlacesField.getText());
    }

    public boolean isModified(TrainListener data) {
        if (nameField.getText() != null ? !nameField.getText().equals(data.getTrainName()) : data.getTrainName() != null)
            return true;
        if (numPlacesField.getText() != null ? !numPlacesField.getText().equals(data.getTrainNumPlaces()) : data.getTrainNumPlaces() != null)
            return true;
        return false;
    }
}
