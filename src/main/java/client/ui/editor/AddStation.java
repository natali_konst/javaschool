package client.ui.editor;

import javax.swing.*;
import java.awt.event.*;

public class AddStation extends JDialog {
    private JPanel contentPane;
    public JButton buttonOK;
    private JButton buttonCancel;
    public JTextField nameField;
    public JTextField abbrField;
    private JPanel Error;
    private JPanel errorField;
    public JLabel errorLabel;

    public AddStation() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
// add your code here
        dispose();
    }

    private void onCancel() {
// add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        AddStation dialog = new AddStation();
        dialog.setTitle("Add Station");
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    public void setData(StationListener data) {
        nameField.setText(data.getStationName());
        abbrField.setText(data.getStationAbbr());
    }

    public void getData(StationListener data) {
        data.setStationName(nameField.getText());
        data.setStationAbbr(abbrField.getText());
    }

    public boolean isModified(StationListener data) {
        if (nameField.getText() != null ? !nameField.getText().equals(data.getStationName()) : data.getStationName() != null)
            return true;
        if (abbrField.getText() != null ? !abbrField.getText().equals(data.getStationAbbr()) : data.getStationAbbr() != null)
            return true;
        return false;
    }
}
