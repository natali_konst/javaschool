package client.ui.editor;

public class TrainListener {
    private String trainName;
    private String trainNumPlaces;

    public TrainListener() {
    }

    public String getTrainName() {
        return trainName;
    }

    public void setTrainName(final String trainName) {
        this.trainName = trainName;
    }

    public String getTrainNumPlaces() {
        return trainNumPlaces;
    }

    public void setTrainNumPlaces(final String trainNumPlaces) {
        this.trainNumPlaces = trainNumPlaces;
    }
}