package client.ui.editor;

public class StationListener {
    private String stationName;
    private String stationAbbr;

    public StationListener() {
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(final String stationName) {
        this.stationName = stationName;
    }

    public String getStationAbbr() {
        return stationAbbr;
    }

    public void setStationAbbr(final String stationAbbr) {
        this.stationAbbr = stationAbbr;
    }
}