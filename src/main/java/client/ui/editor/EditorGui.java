package client.ui.editor;

import com.toedter.calendar.JDateChooser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Date;

public class EditorGui extends JDialog {
    private JPanel contentPane;
    public JButton buttonOK;
    private JButton buttonCancel;
    public JButton addStationButton;
    public JButton addTrainButton;
    public JList listTrains;
    public JPanel AllTickets;
    public JTextField dateFieldPass;
    private JPanel Error;
    public JComboBox comboStations1;
    public JComboBox comboStations2;
    public JButton searchButton;
    private JPanel errorField;
    public JLabel errorLabel;
    private JTabbedPane tabbedPane1;
    public JTextField timetDepField;
    public JTextField dateDepField;
    public JTextField dateArrField;
    public JTextField timetArrField;
    public JComboBox trainCombo;
    public JButton addTimetableButton;

    public EditorGui() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        setTitle("Editor");

//        buttonOK.addActionListener(new ActionListener() {
//            public void actionPerformed(ActionEvent e) {
//                onOK();
//            }
//        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);


        //listTrains.setListData();


    }

    private void onOK() {
// add your code here
        dispose();
    }

    private void onCancel() {
// add your code here if necessary
        dispose();
    }

    public static JPanel buildDatePanel(String label, Date value) {
        JPanel datePanel = new JPanel();

        JDateChooser dateChooser = new JDateChooser();
        if (value != null) {
            dateChooser.setDate(value);
        }
        for (Component comp : dateChooser.getComponents()) {
            if (comp instanceof JTextField) {
                ((JTextField) comp).setColumns(50);
                ((JTextField) comp).setEditable(false);
            }
        }

        datePanel.add(dateChooser);

        SpinnerModel model = new SpinnerDateModel();
        JSpinner timeSpinner = new JSpinner(model);
        JComponent editor = new JSpinner.DateEditor(timeSpinner, "HH:mm:ss");
        timeSpinner.setEditor(editor);
        if (value != null) {
            timeSpinner.setValue(value);
        }

        datePanel.add(timeSpinner);

        return datePanel;
    }

    public static void main(String[] args) {
        EditorGui dialog = new EditorGui();
        dialog.setTitle("Train Timetable");
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
//
    }

//    private void createUIComponents() {
//        // TODO: place custom component creation code here
//
//      JPanel datePanel = buildDatePanel("Time", null);
//      datePanel.setVisible(true);
//    }
}
