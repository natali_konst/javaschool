package client;

import javax.swing.*;

/**
 * Created by Natali on 30/03/14.
 */
public abstract class UiHandler<ResultT> implements Handler<ResultT> {

    protected abstract void process(ResultT result);

    public final void handle(final ResultT result)

    {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                process(result);
            }
        });
    }
}
