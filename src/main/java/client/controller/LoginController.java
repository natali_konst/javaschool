package client.controller;

import client.Client;
import client.Handler;
import client.controller.editor.EditorGuiController;
import client.controller.user.UserEditorGuiController;
import client.ui.LoginGUI;
import client.ui.editor.EditorGui;
import client.ui.user.Timetable;
import common.dto.CredentialsDto;
import common.dto.ErrorDto;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Natali on 30/03/14.
 */
public class LoginController {
    private final Client client;
    private final LoginGUI loginDialog;

    public LoginController(Client client, LoginGUI loginDialog) {
        this.client = client;
        this.loginDialog = loginDialog;

        loginDialog.buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                login();
            }
        });
    }

    private void login() {
        String login = loginDialog.Login.getText();
        char[] password = loginDialog.passwordField1.getPassword();

        client.login(login, new String(password), new Handler<Object>() {
                    @Override
                    public void handle(Object result) {

                        loginDialog.setTitle("OK");

                        if (result instanceof CredentialsDto) {

                            if (((CredentialsDto) result).getRole() == (long) 5) {
                                SwingUtilities.invokeLater(new Runnable() {
                                    @Override
                                    public void run() {
                                        EditorGui editorGui = new EditorGui();
                                        new EditorGuiController(editorGui, client);
                                        editorGui.pack();
                                        editorGui.setVisible(true);
                                    }
                                });
                            } else {

                                SwingUtilities.invokeLater(new Runnable() {
                                    @Override
                                    public void run() {
                                        Timetable timetableGui = new Timetable();
                                        new UserEditorGuiController(timetableGui, client);
                                        timetableGui.pack();
                                        timetableGui.setVisible(true);
                                    }
                                });


                            }


                        }
                        if (result instanceof ErrorDto) {

                            loginDialog.errorLabel.setText(((ErrorDto) result).getError());
                            loginDialog.pack();
                            loginDialog.setVisible(true);

                        }

                    }
                }, new Handler<Throwable>() {
                    @Override
                    public void handle(Throwable result) {
                        loginDialog.setTitle("FAIL");
                    }
                }
        );

//        client.login(login, new String(password), new Runnable() {
//            @Override
//            public void run() {
//
//                loginDialog.setTitle("OK");
//
//                SwingUtilities.invokeLater(new Runnable() {
//                    @Override
//                    public void run() {
//                        EditorGui editorGui = new EditorGui();
//                        new EditorGuiController(editorGui, client);
//                        editorGui.pack();
//                        editorGui.setVisible(true);
//                    }
//                });
//
//            }
//        }, new Runnable() {
//                    @Override
//                    public void run() {
//                                        loginDialog.setTitle("FAIL");
//                    }
//                });
    }
}
