package client.controller.editor;

import client.Client;
import client.UiHandler;
import client.ui.ComboItem;
import client.ui.editor.UpdateTimetable;
import common.command.AllStations;
import common.command.AllTrains;
import common.command.ManageScheduleCommand;
import common.dto.ErrorDto;
import common.dto.StationDto;
import common.dto.TrainDto;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Time;
import java.util.List;

/**
 * Created by Natali on 31/03/14.
 */
public class UpdateTimetableController {

    private final Client client;
    private final UpdateTimetable updateTimetable;
    List<JComboBox> stationsCombo;
    List<TrainDto> trainOptions;
    List<StationDto> stationOptions;

    public UpdateTimetableController(UpdateTimetable updateTimetable, Client client) {

        this.client = client;
        this.updateTimetable = updateTimetable;

        stationsCombo.add(updateTimetable.comboStations1);
        stationsCombo.add(updateTimetable.comboStations2);
        stationsCombo.add(updateTimetable.comboStations3);
        stationsCombo.add(updateTimetable.comboStations4);
        stationsCombo.add(updateTimetable.comboStations5);

        addTrains();
        addStations();

        updateTimetable.buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                submitRequest();
            }
        });


    }

    private void submitRequest() {

        String train = ((ComboItem) updateTimetable.comboTrain.getSelectedItem()).getValue();

        List<String> stationsSelected = null;
        String station1 = ((ComboItem) updateTimetable.comboStations1.getSelectedItem()).getValue();
        stationsSelected.add(station1);
        String station2 = ((ComboItem) updateTimetable.comboStations2.getSelectedItem()).getValue();
        stationsSelected.add(station2);
        String station3 = ((ComboItem) updateTimetable.comboStations3.getSelectedItem()).getValue();
        stationsSelected.add(station3);
        String station4 = ((ComboItem) updateTimetable.comboStations4.getSelectedItem()).getValue();
        stationsSelected.add(station4);
        String station5 = ((ComboItem) updateTimetable.comboStations5.getSelectedItem()).getValue();
        stationsSelected.add(station5);
        stationsSelected.add(station1);

        Time time1 = null;
        Time time2 = null;
        Time time3 = null;
        Time time4 = null;
        Time time5 = null;
        List<Time> times = null;
        time1 = time1.valueOf(updateTimetable.timeField1.getText());
        times.add(time1);
        time2 = time1.valueOf(updateTimetable.timeField2.getText());
        times.add(time2);
        time3 = time1.valueOf(updateTimetable.timeField3.getText());
        times.add(time3);
        time4 = time1.valueOf(updateTimetable.timeField4.getText());
        times.add(time4);
        time5 = time1.valueOf(updateTimetable.timeField5.getText());
        times.add(time5);


        for (int i = 0; i < stationsSelected.size(); i++) {
            if (stationsSelected.get(i) != null) {
                ManageScheduleCommand manageSch = new ManageScheduleCommand(1, stationOptions.get(Integer.parseInt(stationsSelected.get(i))), trainOptions.get(Integer.parseInt(train)), times.get(i));


                client.execute(manageSch, new UiHandler<Object>() {
                            @Override
                            protected void process(Object result) {

                                if (result instanceof ErrorDto) {
                                    updateTimetable.errorLabel.setText(((ErrorDto) result).getError());
                                    updateTimetable.pack();
                                    updateTimetable.setVisible(true);
                                }

//                                else{
//
//                                    success = true;
//                                    Success success = new Success();
//                                    success.setTitle("Successful creation");
//
//                                    success.pack();
//                                    success.setVisible(true);
//                                }

                            }
                        }, new UiHandler<Throwable>() {
                            @Override
                            protected void process(Throwable result) {
                                updateTimetable.errorLabel.setText("There is a problem to connect to Server.");
                                updateTimetable.pack();
                                updateTimetable.setVisible(true);
                            }
                        }
                );


            } else continue;

        }


    }

    protected void addTrains() {

        client.execute(new AllTrains(), new UiHandler<Object>() {

                    @Override
                    protected void process(Object result) {

                        System.out.println("result" + result.toString());

                        if (result instanceof ErrorDto) {
                            updateTimetable.comboTrain.addItem(((ErrorDto) result).getError());
                        } else {
                            System.out.println("result" + result.toString());

                            for (TrainDto o : (List<TrainDto>) result) {
                                System.out.println("O" + o.toString());
                                trainOptions.add(o);
                                updateTimetable.comboTrain.addItem(new ComboItem(o.getTrainNumber(), o.getTrainId().toString()));
                            }

                        }

                    }
                }, new UiHandler<Throwable>() {
                    @Override
                    protected void process(Throwable result) {

                        updateTimetable.errorLabel.setText("There is a problem to connect to Server.");
                        updateTimetable.pack();
                        updateTimetable.setVisible(true);


                    }
                }
        );

    }

    protected void addStations() {

        client.execute(new AllStations(), new UiHandler<Object>() {

                    @Override
                    protected void process(Object result) {

                        System.out.println("result" + result.toString());

                        if (result instanceof ErrorDto) {
                            updateTimetable.comboStations1.addItem(((ErrorDto) result).getError());

                        } else {
                            System.out.println("result" + result.toString());

                            for (StationDto o : (List<StationDto>) result) {
                                System.out.println("O" + o.toString());
                                stationOptions.add(o);
                                for (JComboBox jComboBox : stationsCombo) {
                                    jComboBox.addItem(new ComboItem((o.getStationAbbr() + " - " + o.getStationName()), Long.toString(o.getStationId())));
                                }

                                //updateTimetable.comboStations1.addItem(new ComboItem((o.getStationAbbr() + " - " + o.getStationName()), Long.toString(o.getStationId())));

                            }

                        }

                    }
                }, new UiHandler<Throwable>() {
                    @Override
                    protected void process(Throwable result) {

                        updateTimetable.errorLabel.setText("There is a problem to connect to Server.");
                        updateTimetable.pack();
                        updateTimetable.setVisible(true);

                    }
                }
        );


    }


}
