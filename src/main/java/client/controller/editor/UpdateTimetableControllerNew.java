package client.controller.editor;

/**
 * Created by Natali on 31/03/14.
 */

import client.Client;
import client.UiHandler;
import client.ui.ComboItem;
import client.ui.Success;
import client.ui.editor.UpdateTimetableNew;
import common.command.AllStations;
import common.command.AllTrains;
import common.command.ManageScheduleCommand;
import common.dto.ErrorDto;
import common.dto.StationDto;
import common.dto.TrainDto;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Time;
import java.util.HashSet;
import java.util.List;


/**
 * Created by Natali on 31/03/14.
 */
public class UpdateTimetableControllerNew {

    private final Client client;
    private final UpdateTimetableNew updateTimetable;
    public HashSet<StationDto> stations;
    public HashSet<TrainDto> trains;


    public UpdateTimetableControllerNew(UpdateTimetableNew updateTimetable, Client client) {

        this.client = client;
        this.updateTimetable = updateTimetable;

        addTrains();
        addStations();

        updateTimetable.buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                submitRequest();
            }
        });


    }

    private void submitRequest() {

        String train = ((ComboItem) updateTimetable.comboTrain.getSelectedItem()).getValue();

        String station1 = ((ComboItem) updateTimetable.comboStations1.getSelectedItem()).getValue();

        Time time1 = null;

        time1 = time1.valueOf(updateTimetable.timeField1.getText());
        StationDto stationSelected = null;

        for (StationDto station : stations) {
             if (station.getStationId()== Long.parseLong(station1)){stationSelected = station;}
        }

        TrainDto trainSelected = null;
        for (TrainDto trainDto : trains) {
            if (trainDto.getTrainId()== Long.parseLong(train)){trainSelected = trainDto;}
        }

        ManageScheduleCommand manageSch = new ManageScheduleCommand(1, stationSelected , trainSelected, time1);


        client.execute(manageSch, new UiHandler<Object>() {
                    @Override
                    protected void process(Object result) {

                        if (result instanceof ErrorDto) {
                            updateTimetable.errorLabel.setText(((ErrorDto) result).getError());
                            updateTimetable.pack();
                            updateTimetable.setVisible(true);
                        }

                                else{
                                    Success success = new Success();
                                    success.setTitle("Successful creation");
                                    success.pack();
                                    success.setVisible(true);
                                }

                    }
                }, new UiHandler<Throwable>() {
                    @Override
                    protected void process(Throwable result) {
                        updateTimetable.errorLabel.setText("There is a problem to connect to Server.");
                        updateTimetable.pack();
                        updateTimetable.setVisible(true);
                    }
                }
        );
    }




    protected void addTrains() {


        client.execute(new AllTrains(), new UiHandler<Object>() {

                    @Override
                    protected void process(Object result) {

                        System.out.println("result" + result.toString());

                        if (result instanceof ErrorDto) {
                            updateTimetable.comboTrain.addItem(((ErrorDto) result).getError());
                        } else {
                            System.out.println("result trains - " + result.toString());
                            trains = new HashSet<TrainDto>();

                            //System.out.println(trains);
                            for (TrainDto o : (List<TrainDto>) result) {
                                //System.out.println("O  - here!!!  " + o.getTrainNumber()+ " " + o.getTrainId().toString());
                                trains.add(o);
                                updateTimetable.comboTrain.addItem(new ComboItem(o.getTrainNumber(), o.getTrainId().toString()));

                            }
                        }

                    }
                }, new UiHandler<Throwable>() {
                    @Override
                    protected void process(Throwable result) {

                        updateTimetable.errorLabel.setText("There is a problem to connect to Server.");
                        updateTimetable.pack();
                        updateTimetable.setVisible(true);
                    }
                }
        );

    }

    protected void addStations() {

        client.execute(new AllStations(), new UiHandler<Object>() {

                    @Override
                    protected void process(Object result) {

                        System.out.println("result" + result.toString());

                        if (result instanceof ErrorDto) {
                            updateTimetable.comboStations1.addItem(((ErrorDto) result).getError());

                        } else {
                            System.out.println("result station - " + result.toString());
                            stations = new HashSet<StationDto>();
                            //stations = null;
                            for (StationDto o : (List<StationDto>) result) {
                                //System.out.println("O  - here again!!! "   + o.toString());
                                stations.add(o);

                                updateTimetable.comboStations1.addItem(new ComboItem((o.getStationAbbr() + " - " + o.getStationName()), Long.toString(o.getStationId())));

                            }

                        }

                    }
                }, new UiHandler<Throwable>() {
                    @Override
                    protected void process(Throwable result) {

                        updateTimetable.errorLabel.setText("There is a problem to connect to Server.");
                        updateTimetable.pack();
                        updateTimetable.setVisible(true);

                    }
                }
        );


    }


}

