package client.controller.editor;

import client.Client;
import client.UiHandler;
import client.ui.Success;
import client.ui.editor.AddTrain;
import common.command.ManageTrainCommand;
import common.dto.ErrorDto;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Natali on 30/03/14.
 */
public class AddTrainController {

    private final Client client;
    private final AddTrain addTrain;

    public AddTrainController(AddTrain addTrain, Client client) {

        this.client = client;
        this.addTrain = addTrain;

        addTrain.buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                submitRequest();
            }
        });

    }

    private void submitRequest() {

        String trainName = addTrain.nameField.getText();
        String places = addTrain.numPlacesField.getText();

        ManageTrainCommand manageTrain = new ManageTrainCommand(1, trainName, Integer.parseInt(places));
        client.execute(manageTrain, new UiHandler<Object>() {
                    @Override
                    protected void process(Object result) {

                        if (result instanceof ErrorDto) {
                            addTrain.errorLabel.setText(((ErrorDto) result).getError());
                            addTrain.pack();
                            addTrain.setVisible(true);
                        } else {

                            Success success = new Success();
                            success.setTitle("Successful creation");

                            success.pack();
                            success.setVisible(true);
                        }


                    }
                }, new UiHandler<Throwable>() {
                    @Override
                    protected void process(Throwable result) {
                        addTrain.errorLabel.setText("There is a problem to connect to Server.");
                        addTrain.pack();
                        addTrain.setVisible(true);
                    }
                }
        );

    }


}
