package client.controller.editor;

import client.Client;
import client.UiHandler;
import client.ui.Success;
import client.ui.editor.AddStation;
import common.command.ManageStationCommand;
import common.dto.ErrorDto;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Natali on 30/03/14.
 */
public class AddStationController {

    private final Client client;
    private final AddStation addStation;

    public AddStationController(AddStation addStation, final Client client) {

        this.client = client;
        this.addStation = addStation;

        addStation.buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                submitRequest();
            }
        });

    }

    private void submitRequest() {

        String name = addStation.nameField.getText();
        String abbr = addStation.abbrField.getText();

        ManageStationCommand manageStation = new ManageStationCommand(1, name, abbr);
        client.execute(manageStation, new UiHandler<Object>() {
                    @Override
                    protected void process(Object result) {

                        if (result instanceof ErrorDto) {
                            addStation.errorLabel.setText(((ErrorDto) result).getError());
                            addStation.pack();
                            addStation.setVisible(true);
                        } else {

                            Success success = new Success();
                            success.setTitle("Successful creation");

                            success.pack();
                            success.setVisible(true);
                        }


                    }
                }, new UiHandler<Throwable>() {
                    @Override
                    protected void process(Throwable result) {
                        addStation.errorLabel.setText("There is a problem to connect to Server.");
                        addStation.pack();
                        addStation.setVisible(true);
                    }
                }
        );

    }


}
