package client.controller.editor;

import client.Client;
import client.UiHandler;
import client.ui.ComboItem;
import client.ui.editor.*;
import common.DateUtils;
import common.command.*;
import common.dto.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Time;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * Created by Natali on 30/03/14.
 */
public class EditorGuiController {

    private final Client client;
    private final EditorGui editorGui;

    public EditorGuiController(EditorGui editorGui, final Client client) {

        this.client = client;
        this.editorGui = editorGui;

        editorGui.buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showPassengers();      //todo add action
            }
        });

        editorGui.searchButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                findTimetable();
            }
        });

        editorGui.addStationButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                AddStation addStation = new AddStation();
                new AddStationController(addStation, client);
                addStation.setTitle("Creation of new station");
                addStation.pack();
                addStation.setVisible(true);
            }
        });

        editorGui.addTrainButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {


                AddTrain addTrain = new AddTrain();
                new AddTrainController(addTrain, client);
                addTrain.setTitle("Addition of new train");
                addTrain.pack();
                addTrain.setVisible(true);
            }
        });

        editorGui.addTimetableButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

//                 //todo

                UpdateTimetableNew updateTimetable = new UpdateTimetableNew();
                new UpdateTimetableControllerNew(updateTimetable, client);
                updateTimetable.setTitle("Change schedule");
                updateTimetable.pack();
                updateTimetable.setVisible(true);

//                UpdateTimetable updateTimetable = new UpdateTimetable();
//                new UpdateTimetableController(updateTimetable, client);
//                updateTimetable.setTitle("Change schedule");
//                updateTimetable.pack();
//                updateTimetable.setVisible(true);
            }
        });

        addTrains();
        addStations();


//        Object item = editorGui.comboStations1.getSelectedItem();
//        String value = ((ComboItem) item).getValue();
//
//        System.out.println(value);

    }

    private void showPassengers() {

        Date date = null;
        try {
            date = DateUtils.fromStringToDate(editorGui.dateFieldPass.getText());
            Object item = editorGui.trainCombo.getSelectedItem();
            String value = ((ComboItem) item).getValue();
            System.out.println(" value - " + value);

            TicketsTrainDate ticketsTrainDate = new TicketsTrainDate(date, Long.parseLong(value));

            client.execute(ticketsTrainDate, new UiHandler<Object>() {
                        @Override
                        protected void process(Object result) {
                            System.out.println("sucess");

                            if (result instanceof ErrorDto) {
                                editorGui.errorLabel.setText(((ErrorDto) result).getError());
                                editorGui.pack();
                                editorGui.setVisible(true);
                            } else {

                                Results resultsGui = new Results();
                                resultsGui.setTitle("List of Passengers");
                                for (TicketDto o : (List<TicketDto>) result) {

                                    resultsGui.textArea.append("\n");
                                    resultsGui.textArea.append(o.toString());
                                    resultsGui.textArea.append("\n");
                                }
                                resultsGui.pack();
                                resultsGui.setVisible(true);
                            }
                        }
                    }, new UiHandler<Throwable>() {
                        @Override
                        protected void process(Throwable result) {

                            editorGui.errorLabel.setText("There is a problem to connect to Server.");
                            editorGui.pack();
                            editorGui.setVisible(true);
                        }
                    }
            );
        } catch (ParseException e) {
            e.printStackTrace();
            editorGui.errorLabel.setText("Please, enter date correctly!");
            editorGui.pack();
            editorGui.setVisible(true);
        }
    }

    private void findTimetable() {


        Time time = null;
        //editorGui.timetDepField.getText();

        Object station1 = editorGui.comboStations1.getSelectedItem();
        Object station2 = editorGui.comboStations2.getSelectedItem();

        String station1Value = ((ComboItem) station1).getValue();
        String station2Value = ((ComboItem) station2).getValue();

        if (editorGui.timetDepField.getText().isEmpty()) {

            GetStationTimetable stationTimetable = new GetStationTimetable(Long.parseLong(station1Value));

            client.execute(stationTimetable, new UiHandler<Object>() {
                        @Override
                        protected void process(Object result) {
                            System.out.println("sucess here!");

                            if (result instanceof ErrorDto) {
                                editorGui.errorLabel.setText(((ErrorDto) result).getError());
                                editorGui.pack();
                                editorGui.setVisible(true);
                            } else {

                                Results resultsGui = new Results();
                                resultsGui.setTitle("List of Trains");
                                //System.out.println("result! - " + result.toString());

                                for (ScheduleDto o : (List<ScheduleDto>) result) {

                                    resultsGui.textArea.append("\n");
                                    resultsGui.textArea.append(o.toString());
                                    resultsGui.textArea.append("\n");
                                }
                                resultsGui.pack();
                                resultsGui.setVisible(true);
                            }
                        }
                    }, new UiHandler<Throwable>() {
                        @Override
                        protected void process(Throwable result) {

                            editorGui.errorLabel.setText("There is a problem to connect to Server.");
                            editorGui.pack();
                            editorGui.setVisible(true);
                        }
                    }
            );


        } else {
            time = time.valueOf(editorGui.timetDepField.getText());
            TimetableTimeStations timetableCommand = new TimetableTimeStations(time, Long.parseLong(station1Value), Long.parseLong(station2Value), true);

            client.execute(timetableCommand, new UiHandler<Object>() {
                        @Override
                        protected void process(Object result) {
                            System.out.println("sucess here!");

                            if (result instanceof ErrorDto) {
                                editorGui.errorLabel.setText(((ErrorDto) result).getError());
                                editorGui.pack();
                                editorGui.setVisible(true);
                            } else {

                                Results resultsGui = new Results();
                                resultsGui.setTitle("List of Trains");
                                //System.out.println("result! - " + result.toString());

                                for (ScheduleDto o : (List<ScheduleDto>) result) {

                                    resultsGui.textArea.append("\n");
                                    resultsGui.textArea.append(o.toString());
                                    resultsGui.textArea.append("\n");
                                }
                                resultsGui.pack();
                                resultsGui.setVisible(true);
                            }
                        }
                    }, new UiHandler<Throwable>() {
                        @Override
                        protected void process(Throwable result) {

                            editorGui.errorLabel.setText("There is a problem to connect to Server.");
                            editorGui.pack();
                            editorGui.setVisible(true);
                        }
                    }
            );

        }

    }

    private void addTrains() {

        client.execute(new AllTrains(), new UiHandler<Object>() {

                    @Override
                    protected void process(Object result) {

                        System.out.println("result" + result.toString());

                        if (result instanceof ErrorDto) {
                            editorGui.trainCombo.addItem(((ErrorDto) result).getError());
                        } else {
                            System.out.println("result" + result.toString());

                            for (TrainDto o : (List<TrainDto>) result) {
                                System.out.println("O" + o.toString());
                                editorGui.trainCombo.addItem(new ComboItem(o.getTrainNumber(), o.getTrainId().toString()));
                            }

                        }

                    }
                }, new UiHandler<Throwable>() {
                    @Override
                    protected void process(Throwable result) {

                        editorGui.errorLabel.setText("There is a problem to connect to Server.");
                        editorGui.pack();
                        editorGui.setVisible(true);


                    }
                }
        );

    }


    private void addStations() {

        client.execute(new AllStations(), new UiHandler<Object>() {

                    @Override
                    protected void process(Object result) {

                        System.out.println("result" + result.toString());

                        if (result instanceof ErrorDto) {
                            editorGui.comboStations1.addItem(((ErrorDto) result).getError());
                            editorGui.comboStations2.addItem(((ErrorDto) result).getError());
                        } else {
                            System.out.println("result" + result.toString());

                            for (StationDto o : (List<StationDto>) result) {
                                System.out.println("O" + o.toString());

                                editorGui.comboStations1.addItem(new ComboItem((o.getStationAbbr() + " - " + o.getStationName()), Long.toString(o.getStationId())));
                                editorGui.comboStations2.addItem(new ComboItem((o.getStationAbbr() + " - " + o.getStationName()), Long.toString(o.getStationId())));
                            }

                        }

                    }
                }, new UiHandler<Throwable>() {
                    @Override
                    protected void process(Throwable result) {

                        editorGui.errorLabel.setText("There is a problem to connect to Server.");
                        editorGui.pack();
                        editorGui.setVisible(true);

                    }
                }
        );


    }

}
