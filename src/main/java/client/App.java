package client;

import client.controller.LoginController;
import client.ui.LoginGUI;

import javax.swing.*;

/**
 * Created by Natali on 29/03/14.
 */
public class App implements Runnable {

    public static void main(final String[] args) {


        final Client client = new Client("localhost", 9900);

        final LoginGUI loginUI = new LoginGUI();
        loginUI.pack();
        new LoginController(client, loginUI);

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                loginUI.setVisible(true);
            }
        });

        //client.stopClient();


    }

    @Override
    public void run() {

        final Client client = new Client("localhost", 9900);

        final LoginGUI loginUI = new LoginGUI();
        loginUI.pack();
        new LoginController(client, loginUI);


        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                loginUI.setVisible(true);
            }
        });

    }
}
