package common;

import java.io.Serializable;

/**
 * Created by Natali on 20/03/14.
 */
public class TestObject implements Serializable {


    int value;
    String id;

    public TestObject(int v, String s) {
        this.value = v;
        this.id = s;
    }

    @Override
    public String toString() {
        return "value=" + value +
                ", id='" + id;
    }
}
