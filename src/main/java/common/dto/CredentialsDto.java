package common.dto;


import server.models.Role;

import java.io.Serializable;

/**
 * Created by Natali on 27/03/14.
 */
public class CredentialsDto implements Serializable {

    private long role;

    public CredentialsDto(Role role) {
        this.role = role.getId();
    }

    public long getRole() {
        return role;
    }
}
