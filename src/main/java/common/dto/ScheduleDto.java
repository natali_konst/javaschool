package common.dto;

import java.io.Serializable;
import java.sql.Time;

/**
 * Created by Natali on 27/03/14.
 */
public class ScheduleDto implements Serializable {


    private long scheduleId;
    private StationDto stationId;
    private TrainDto trainId;
    private Time time;

    public ScheduleDto(long scheduleId, StationDto stationId, TrainDto trainId, Time time) {
        this.scheduleId = scheduleId;
        this.stationId = stationId;
        this.trainId = trainId;
        this.time = time;
    }

    public long getScheduleId() {
        return scheduleId;
    }

    public StationDto getStationId() {
        return stationId;
    }

    public TrainDto getTrainId() {
        return trainId;
    }

    public Time getTime() {
        return time;
    }


    @Override
    public String toString() {
        return "Time =  " + time.toString() +
                " , trainId =  " + trainId.getTrainNumber() + " , departure station =  " + stationId.getStationName() + " ";
    }
}
