package common.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Natali on 27/03/14.
 */
public class TicketDto implements Serializable {


    private Date datePurchase;
    private Date dateDep;
    private TrainDto trainId;
    private StationDto statDepId;
    private StationDto statArrId;
    private UserDto userId;
    private boolean Status;

    public TicketDto(UserDto userId, Date datePurchase, Date dateDep, TrainDto trainId, StationDto statDepId, StationDto statArrId) {
        this.userId = userId;
        this.datePurchase = datePurchase;
        this.dateDep = dateDep;
        this.trainId = trainId;
        this.statDepId = statDepId;
        this.statArrId = statArrId;
    }

    public Date getDatePurchase() {
        return datePurchase;
    }

    public Date getDateDep() {
        return dateDep;
    }

    public TrainDto getTrainId() {
        return trainId;
    }

    public StationDto getStatDepId() {
        return statDepId;
    }

    public StationDto getStatArrId() {
        return statArrId;
    }

    public UserDto getUserId() {
        return userId;
    }

    public boolean isStatus() {
        return Status;
    }

    @Override
    public String toString() {
        return "User Id =  " + userId.getId() + ", User name = " + userId.getName() + " , User surname = " + userId.getSurname() + ", Date of birth = " + userId.getDateOfBirth().toString().substring(0,10) + " ";
    }
}
