package common.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Natali on 27/03/14.
 */
public class UserDto implements Serializable {

    private long id;

    private String email;

    private int role;

    private String name;

    private String surname;

    private Date dateOfBirth;

    public UserDto(long id, int role) {
        this.id = id;
        this.role = role;
    }

    public long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public int getRole() {
        return role;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}
