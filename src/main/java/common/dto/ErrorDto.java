package common.dto;

import java.io.Serializable;

/**
 * Created by Natali on 27/03/14.
 */
public class ErrorDto implements Serializable {

    private String error;

    public ErrorDto(String error) {
        this.error = error;
    }


    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
