package common.dto;

import java.io.Serializable;

/**
 * Created by Natali on 27/03/14.
 */
public class TrainDto implements Serializable {


    private Long trainId;
    private String trainNumber;
    private long numberPlaces;

    public TrainDto(Long trainId, String trainNumber, long numberPlaces) {
        this.trainId = trainId;
        this.trainNumber = trainNumber;
        this.numberPlaces = numberPlaces;
    }

    public Long getTrainId() {
        return trainId;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public long getNumberPlaces() {
        return numberPlaces;
    }
}
