package common.dto;

import java.io.Serializable;

/**
 * Created by Natali on 27/03/14.
 */
public class StationDto implements Serializable {

    private long stationId;
    private String stationName;
    private String stationAbbr;

    public StationDto(long stationId, String stationName, String stationAbbr) {
        this.stationId = stationId;
        this.stationName = stationName;
        this.stationAbbr = stationAbbr;
    }

    public long getStationId() {
        return stationId;
    }

    public String getStationName() {
        return stationName;
    }

    public String getStationAbbr() {
        return stationAbbr;
    }
}
