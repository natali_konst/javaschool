package common;

/**
 * Created by Natali on 25/03/14.
 */
public enum CommandKeyEnum {

    LOGIN,    // true/false - find this login - find id - find passw - if match = OK
    REGISTER,  // add User - login+passw (check login - if exists - return)
    //CHANGEPROFILE,

    GETTRAINS, //all existing - list +
    GETSTATIONS,  // all existing  +

    GETTIMETABLEDEP, //  time, 2 stations ids
    GETTIMETABLEARRIV,              //

    STATIONTIMETABLE, // get all trains per station  +

    BUYTICKET,    //
    // find bought tickets by date and train, compare number of sold to id_train - num places
    // find all users with all data (FIO)
    // add tickets + update User
    //

    //CANCELTICKET,
    //SHOWUSERTICKETS

    ADDSTATION,     // take st abbr and stationname - add  +
    DELETESTATION,     //delete by key +

    ADDTRAIN,     //+ take number,num places and add  +
    DELETETRAIN,  // delete by id  +

    SHOWTICKETS,//for a train and date + select all users from there
    SHOWTRAINS //on this day  ???


}
