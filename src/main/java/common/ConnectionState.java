package common;

/**
 * Created by Natali on 20/03/14.
 */
public enum ConnectionState {
    UNKNOWN(-1),
    CONNECTING(0),
    CONNECTED(1),
    CLOSING(2),
    CLOSED(3);

    private int state;

    private ConnectionState(int state) {
        this.state = state;
    }

    public int state() {
        return state;
    }

    public static ConnectionState fromInt(int val) {
        switch (val) {
            case 0:
                return CONNECTING;
            case 1:
                return CONNECTED;
            case 2:
                return CLOSING;
            case 3:
                return CLOSED;
            default:
                return UNKNOWN;
        }
    }
}