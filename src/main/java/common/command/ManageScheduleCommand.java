package common.command;

import common.Command;
import common.dto.StationDto;
import common.dto.TrainDto;

import java.sql.Time;

/**
 * Created by Natali on 27/03/14.
 */
public class ManageScheduleCommand implements Command {


    int action; //1-add, 2 - delete, 3 - update
    Long scheduleId;
    StationDto stationId;
    TrainDto trainId;
    Time time;

    public ManageScheduleCommand(int action, StationDto stationId, TrainDto trainId, Time time) {
        this.action = action;
        this.stationId = stationId;
        this.trainId = trainId;
        this.time = time;
    }

    public ManageScheduleCommand(int action, Long scheduleId, StationDto stationId, TrainDto trainId, Time time) {
        this.action = action;
        this.scheduleId = scheduleId;
        this.stationId = stationId;
        this.trainId = trainId;
        this.time = time;
    }

    public int getAction() {
        return action;
    }

    public Long getScheduleId() {
        return scheduleId;
    }

    public StationDto getStationId() {
        return stationId;
    }

    public TrainDto getTrainId() {
        return trainId;
    }

    public Time getTime() {
        return time;
    }
}
