package common.command;

import common.Command;

/**
 * Created by Natali on 27/03/14.
 */
public class GetStationTimetable implements Command {

    Long stationId;

    public GetStationTimetable(Long stationId) {
        this.stationId = stationId;
    }

    public Long getStationId() {
        return stationId;
    }

}
