package common.command;

import common.Command;

import java.sql.Time;

/**
 * Created by Natali on 27/03/14.
 */
public class TimetableTimeStations implements Command {

    boolean departure;
    Time time;
    Long statDepId;
    Long statArrId;

    public TimetableTimeStations(Time time, Long statDepId, Long statArrId, boolean departure) {
        this.time = time;
        this.statDepId = statDepId;
        this.statArrId = statArrId;
        this.departure = departure;
    }

    public boolean isDeparture() {
        return departure;
    }

    public Time getTime() {
        return time;
    }

    public Long getStatDepId() {
        return statDepId;
    }

    public Long getStatArrId() {
        return statArrId;
    }
}
