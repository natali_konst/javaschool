package common.command;

import common.Command;

/**
 * Created by Natali on 27/03/14.
 */
public class LoginCommand implements Command {

    String login;
    String password;

    public LoginCommand(String login, String password) {
        this.login = login;
        this.password = password;
    }


    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
