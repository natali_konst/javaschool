package common.command;

import common.Command;

import java.util.Date;

/**
 * Created by Natali on 27/03/14.
 */
public class TicketsTrainDate implements Command {

    Date date;
    Long trainId;


    public TicketsTrainDate(Date date, Long trainId) {
        this.date = date;
        this.trainId = trainId;
    }

    public Date getDate() {
        return date;
    }

    public Long getTrainId() {
        return trainId;
    }
}
