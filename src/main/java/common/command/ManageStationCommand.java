package common.command;

import common.Command;

/**
 * Created by Natali on 27/03/14.
 */
public class ManageStationCommand implements Command {

    int action; //1-add, 2 - delete, 3 - update
    Long stationId;
    String stationName;
    String stationAbbr;

    public ManageStationCommand(int action, String stationName, String stationAbbr) {
        this.action = action;
        this.stationName = stationName;
        this.stationAbbr = stationAbbr;
    }

    public ManageStationCommand(int action, Long stationId, String stationName, String stationAbbr) {
        this.action = action;
        this.stationId = stationId;
        this.stationName = stationName;
        this.stationAbbr = stationAbbr;
    }

    public int getAction() {
        return action;
    }

    public Long getStationId() {
        return stationId;
    }

    public String getStationName() {
        return stationName;
    }

    public String getStationAbbr() {
        return stationAbbr;
    }
}
