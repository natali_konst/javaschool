package common.command;

import common.Command;

import java.sql.Time;
import java.util.Date;

/**
 * Created by Natali on 27/03/14.
 */
public class TicketSell implements Command {

    long userId;
    Time timeOfTrain; //
    Date datePurchase;
    Date dateDep;
    long trainId;
    long statDepId;
    long statArrId;

    String name;
    String surname;
    Date dateOfBirth;

    public TicketSell() {
    }

    public TicketSell(long userId, Time timeOfTrain, Date datePurchase, Date dateDep, long trainId, long statDepId, long statArrId, String name, String surname, Date dateOfBirth) {
        this.userId = userId;
        this.timeOfTrain = timeOfTrain;
        this.datePurchase = datePurchase;
        this.dateDep = dateDep;
        this.trainId = trainId;
        this.statDepId = statDepId;
        this.statArrId = statArrId;
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Time getTimeOfTrain() {
        return timeOfTrain;
    }

    public void setTimeOfTrain(Time timeOfTrain) {
        this.timeOfTrain = timeOfTrain;
    }

    public Date getDatePurchase() {
        return datePurchase;
    }

    public void setDatePurchase(Date datePurchase) {
        this.datePurchase = datePurchase;
    }

    public Date getDateDep() {
        return dateDep;
    }

    public void setDateDep(Date dateDep) {
        this.dateDep = dateDep;
    }

    public long getTrainId() {
        return trainId;
    }

    public void setTrainId(long trainId) {
        this.trainId = trainId;
    }

    public long getStatDepId() {
        return statDepId;
    }

    public void setStatDepId(long statDepId) {
        this.statDepId = statDepId;
    }

    public long getStatArrId() {
        return statArrId;
    }

    public void setStatArrId(long statArrId) {
        this.statArrId = statArrId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}
