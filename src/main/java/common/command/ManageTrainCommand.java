package common.command;

import common.Command;

/**
 * Created by Natali on 27/03/14.
 */
public class ManageTrainCommand implements Command {

    int action; //1-add, 2 - delete, 3 - update
    Long trainId;
    String trainNumber;
    int numberPlaces;


    public ManageTrainCommand(int action, Long trainId, String trainNumber, int numberPlaces) {
        this.action = action;
        this.trainId = trainId;
        this.trainNumber = trainNumber;
        this.numberPlaces = numberPlaces;
    }

    public ManageTrainCommand(int action, String trainNumber, int numberPlaces) {

        this.action = action;
        this.trainNumber = trainNumber;
        this.numberPlaces = numberPlaces;
    }

    public int getAction() {
        return action;
    }

    public Long getTrainId() {
        return trainId;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public int getNumberPlaces() {
        return numberPlaces;
    }
}
