package common;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Natali on 29/03/14.
 */
public class DateUtils {

    public static Date fromStringToDate(String inputString) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        return dateFormat.parse(inputString);

    }
//
//    public static Date setDateOfMonth(int year, String month, int day){
//
//        Date date = new Date();
//
//        date = DateUtils.setYears(date, 2012);
//        date = DateUtils.setMonths(date, JUNE);
//        date = DateUtils.setDays(date, 3);
//        date = DateUtils.truncate(date, DAY_OF_MONTH);
//
//    }

    public static java.sql.Time utilDateToSqlTime(java.util.Date utilDate) {
        return new java.sql.Time(utilDate.getTime());
    }

    public static java.sql.Date utilDateToSqlDate(java.util.Date utilDate) {
        return new java.sql.Date(utilDate.getTime());
    }


}
